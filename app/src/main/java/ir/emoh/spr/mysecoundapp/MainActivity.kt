package ir.emoh.spr.mysecoundapp

import android.content.Intent
import android.graphics.drawable.ColorDrawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.support.v4.app.NotificationCompat.getExtras
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    companion object {
        val COLOR = "COLOR"
        val TEXTBUTTON = "TEXTBUTTON"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val Button1 = findViewById<Button>(R.id.button1)
        val Button2 = findViewById<Button>(R.id.button2)
        val Button3 = findViewById<Button>(R.id.button3)
        val Button4 = findViewById<Button>(R.id.button4)
        val Button5 = findViewById<Button>(R.id.button5)
        val Button6 = findViewById<Button>(R.id.button6)
        Button1.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button1.background as ColorDrawable).color
            val textButton = Button1.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
        Button2.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button2.background as ColorDrawable).color
            val textButton = Button2.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
        Button3.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button3.background as ColorDrawable).color
            val textButton = Button3.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
        Button4.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button4.background as ColorDrawable).color
            val textButton = Button4.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
        Button5.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button5.background as ColorDrawable).color
            val textButton = Button5.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
        Button6.setOnClickListener {
            val intent = Intent(this, secound_activity::class.java)
            val color: Int = (Button6.background as ColorDrawable).color
            val textButton = Button6.text.toString()
            intent.putExtra(TEXTBUTTON,textButton)
            intent.putExtra(COLOR,color)
            startActivity(intent)
        }
    }
}

