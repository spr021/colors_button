package ir.emoh.spr.mysecoundapp

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.TextView
import ir.emoh.spr.mysecoundapp.MainActivity.Companion.COLOR
import android.R.attr.data



class secound_activity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_secound_activity)
        val textview = findViewById<TextView>(R.id.secound_activity)
        val color = intent.extras.getInt("COLOR")
        val textButton = intent.extras.getString("TEXTBUTTON")
        textview.setBackgroundColor(color)
        textview.setText(textButton)
    }
}
